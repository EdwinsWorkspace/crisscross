Rails.application.routes.draw do


  ## - - - - - - - - - - - Static pages - - - - - - - - - - ##
  root 'crisscross#home'

  get 'crisscross/about'
end
